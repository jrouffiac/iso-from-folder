﻿; 	Small DIsk Utilities 
;	Copyright 2016 - 2018 Jean-Yves Rouffiac
;{ 
; This Source Code Form is subject To the terms of the Mozilla Public License, v. 2.0. 
; If a copy of the MPL was Not distributed With this file, You can obtain one at http://mozilla.org/MPL/2.0/.
;}

; Module to handle disk utilities - OS X / macOS

Module _DiskUtilities
	
	EnableExplicit
	
	; Create the ISO
	Procedure.b CreateISO(sourceFolderPath.s, outputFolderPath.s = "", outputFileName.s = "")
		Debug ">>> _DiskUtilities:CreateISO()"
		
		Protected shellProgram.i = 0
		Protected parameters.s = ""
		Protected programOutput.s = ""
		Protected error.s = ""
		Protected success.b = #False
			
		If outputFolderPath = ""
			outputFolderPath = GetHomeDirectory() + "Desktop"
		EndIf
		
		If outputFileName = ""
			outputFileName = "output.iso"
		EndIf
		
		If Not _FileSystem::IsFolder(sourceFolderPath)
			Debug("Error: Source folder " + sourceFolderPath + " was not found")
			ProcedureReturn #False
		EndIf
		
		If Not _FileSystem::IsFolder(outputFolderPath)
			Debug("Error: Target folder " + outputFolderPath + " was not found")
			ProcedureReturn #False
		EndIf
		
		parameters = "makehybrid -o " + outputFolderPath + "/" + outputFileName + " " + sourceFolderPath + " -iso -joliet "
		
		; test
		;shellProgram = RunProgram("ls", "-al " + sourceFolderPath, "", #PB_Program_Open | #PB_Program_Read)
		
		; will run for example: 
		;hdiutil makehybrid -o ~/Desktop/output.iso ~/path/to/folder/to/be/converted -iso -joliet		
		shellProgram = RunProgram("hdiutil", parameters, "", #PB_Program_Open | #PB_Program_Read | #PB_Program_Error)
		
		If shellProgram
			
			Debug "RUNNING: hdiutil " + parameters
			
			While ProgramRunning(shellProgram)
				
				error = ReadProgramError(shellProgram)
				If Not error = ""
					Debug "ERROR: " + error
					Break
				EndIf
				
				If AvailableProgramOutput(shellProgram)
					programOutput = ReadProgramString(shellProgram)
					Debug "-> " + programOutput
				EndIf
			Wend
			
			Debug "No more output from shell program"
			
			CloseProgram(shellProgram)
			
			success = #True
		Else
			Debug("Error: Unable to run hdiutil")
		EndIf
		
		ProcedureReturn success
		
	EndProcedure
	
	
	; Create an empty floppy disk image, for use, eg, in VirtualBox, VMWare etc
	; dd if=/dev/zero of=myfloppy.img bs=1024 count=1440 
	; TODO make the disk type parameters user configurable
	Procedure.b CreateEmptyFloppyImage(blocks.i, blockSize.i, outputFileName.s = "", outputFolderPath.s = "")
		Debug ">>> _DiskUtilities:CreateEmptyFloppyImage()"
		
		Protected shellProgram.i  = 0
		Protected parameters.s = ""
		Protected programOutput.s = ""
		Protected error.s = ""
		Protected success.b = #False
		
		If outputFileName = ""
			outputFileName = "myfloppy.img"
		EndIf
		
		If outputFolderPath = ""
			outputFolderPath = GetHomeDirectory() + "Desktop"
		EndIf
		
		If Not _FileSystem::IsFolder(outputFolderPath)
			Debug("Error: Target folder " + outputFolderPath + " was not found")
			ProcedureReturn #False
		EndIf
		
		parameters = "if=/dev/zero of=" + outputFolderPath + "/" + outputFileName + " bs=" + Str(blockSize) + " count=" + Str(blocks)
		
		shellProgram = RunProgram("dd", parameters, "", #PB_Program_Open | #PB_Program_Read | #PB_Program_Error)
		
		If shellProgram
			
			Debug "RUNNING: dd " + parameters
			
			While ProgramRunning(shellProgram)
				
				error = ReadProgramError(shellProgram)
				If Not error = ""
					Debug "ERROR: " + error
					Break
				EndIf
				
				If AvailableProgramOutput(shellProgram)
					programOutput = ReadProgramString(shellProgram)
					Debug "-> " + programOutput
				EndIf
			Wend
			
			Debug "No more output from shell program"
			
			CloseProgram(shellProgram)
			
			success = #True
		Else
			Debug("Error: Unable to run dd")
		EndIf
		
		ProcedureReturn success
		
	EndProcedure
	
EndModule
