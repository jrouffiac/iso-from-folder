﻿; 	Small DIsk Utilities 
;	Copyright 2016 - 2018 Jean-Yves Rouffiac
;{ 
; This Source Code Form is subject To the terms of the Mozilla Public License, v. 2.0. 
; If a copy of the MPL was Not distributed With this file, You can obtain one at http://mozilla.org/MPL/2.0/.
;}


; Declaration of public file system functions

DeclareModule _FileSystem
	
	Declare.b IsFolder(path.s)
	Declare.b FileExists(fileName.s)
	
EndDeclareModule
