; 	Small DIsk Utilities 
;	Copyright 2016 - 2018 Jean-Yves Rouffiac
;{ 
; This Source Code Form is subject To the terms of the Mozilla Public License, v. 2.0. 
; If a copy of the MPL was Not distributed With this file, You can obtain one at http://mozilla.org/MPL/2.0/.
;}


; Commonly used values
; should be included before all other modules
DeclareModule _Common
	
	#SYSTEM_Name      = "Small Disk Utilities"
	#SYSTEM_Version   = "1.2.5"
	#SYSTEM_Author    = "Jean-Yves Rouffiac"
	#SYSTEM_Copyright = "© Copyright 2016 - 2021" + #SYSTEM_Author
	#SYSTEM_License   = "Mozilla Public License 2.0"
	
	; instantiate enumerations used by other modules
	Enumeration Windows 0
	EndEnumeration

	Enumeration Controls 0
	EndEnumeration

	Enumeration CustomEvents #PB_Event_FirstCustomValue
		#EVENT_Quit
	EndEnumeration
	
	Enumeration PBMenuEvents
		#EVENTMENU_quit = -4	
	EndEnumeration
	
EndDeclareModule

Module _Common
EndModule
	
